# AUTHOR: ERIC MUSSELMAN
# Duke University
# Durham, NC
# 2021

import os
import matplotlib.pyplot as plt 
import numpy as np 

model = 0
fiber = 0

ve_filename = os.path.join('..', 've_files', f'model{model}_fiber{fiber}.dat')
ve = np.loadtxt(ve_filename, skiprows=1)
ve*=1000 # convert to mV (comsol gave us Volts)

coords_filename = os.path.join('..', 'coords_files', f'{fiber}.dat')
coords = np.loadtxt(coords_filename, skiprows=1, usecols=2)
coords*=0.001 # convert to mm

plt.figure()
plt.plot(coords, ve[:])
plt.ylabel('$V_e$(z) (mV)')
plt.xlabel('Nerve Length (mm)')
plt.xticks([0, 0.5*np.max(coords), np.max(coords)])
plt.show()

print('=========== DONE ===========')